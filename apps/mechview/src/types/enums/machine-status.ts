export enum MachineStatus {
  idle = 'idle',
  running = 'running',
  finished = 'finished',
  errored = 'errored', // errorred?
}
