import {MachineStatus} from '../enums/machine-status';

export interface MachineEventSample {
  machine_id: string;
  id: string;
  timestamp: string;
  status: MachineStatus;
}
