import {MachineStatus} from '../enums/machine-status';

export interface MachineEvent {
  timestamp: string
  status: MachineStatus
}
