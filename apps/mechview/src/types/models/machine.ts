import {MachineStatus} from '../enums/machine-status';
import {MachineType} from '../enums/machine-type';
import {MachineEvent} from './machine-event';

export interface Machine {
  id: string;
  status: MachineStatus;
  machine_type: MachineType;
  longitude: number;
  latitude: number;
  last_maintenance: string;
  install_date: string;
  floor: number;
  events: MachineEvent[];
  timestamp?: string;
}
