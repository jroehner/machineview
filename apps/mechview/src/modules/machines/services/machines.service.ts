import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, EMPTY, Observable, of} from 'rxjs';
import {Socket} from 'phoenix';

import {Machine} from '../../../types/models/machine';
import {MachineEventSample} from '../../../types/models/machine-event-sample';
import {catchError, map} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MachinesService {
  readonly host = 'https://machinestream.herokuapp.com';
  readonly urlEventsSocket = `wss://machinestream.herokuapp.com/api/v1/events`;

  private socket: Socket;
  private machineSampleEventsSubject: BehaviorSubject<MachineEventSample | null> = new BehaviorSubject<MachineEventSample | null>(null);

  machineSampleEvents$ = this.machineSampleEventsSubject.asObservable();

  private urlMachines = () => `${this.host}/api/v1/machines`;
  private urlMachine = (id: string) => `${this.host}/api/v1/machines/${id}`;

  constructor(private httpClient: HttpClient) {
    this.socket = new Socket(this.urlEventsSocket);
    this.connect();
  }

  getAll(): Observable<Machine[]> {
    return <Observable<Machine[]>>this.httpClient.get(this.urlMachines()).pipe(
      map(this.transformResponse),
      map(this.addTimestamps),
    );
  }

  getOne(id: string): Observable<Machine> {
    return <Observable<Machine>>this.httpClient.get(this.urlMachine(id)).pipe(
      map(this.transformResponse),
      map(this.addTimestamp),
    );
  }

  connect() {
    this.socket.connect();

    const channel = this.socket.channel('events', {});
    channel.join();
    channel.on('new', this.onMachineEventSample);
  }

  onMachineEventSample = (sample: MachineEventSample) =>
    this.machineSampleEventsSubject.next(sample);

  disconnect() {
    this.socket.disconnect();
  }

  private transformResponse = (response: any) => response.data;

  private addTimestamps = (machines: Machine[]): Machine[] =>
    machines.map(machine => ({...machine, timestamp: Date()}))

  private addTimestamp = (machine: Machine): Machine => ({
    ...machine,
    timestamp: Date()
  })
}
