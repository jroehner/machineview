import {Component, NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {DashboardComponent} from './components/dashboard/dashboard.component';
import {DetailViewComponent} from './components/detail-view/detail-view.component';


const routes: Routes = [
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {path: 'dashboard/:id', component: DetailViewComponent},
  {path: 'dashboard', component: DashboardComponent},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MachinesRoutingModule {
}
