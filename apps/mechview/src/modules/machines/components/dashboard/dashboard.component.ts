import {Component, OnInit} from '@angular/core';
import {MachinesService} from '../../services/machines.service';
import {combineLatest, Observable} from 'rxjs';
import {Machine} from '../../../../types/models/machine';
import {MachineEventSample} from '../../../../types/models/machine-event-sample';
import {map} from 'rxjs/operators';

@Component({
  selector: 'zeiss-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  liveMachine$: Observable<Machine[]> = new Observable<Machine[]>();
  machineStore: Machine[] | null = null;

  displayedColumns: string[] = [
    'id', 'machine_type', 'status', 'timestamp', 'floor', 'install_date', 'last_maintenance', 'longitude', 'latitude',
  ]

  constructor(private machineService: MachinesService) {
  }

  ngOnInit(): void {
    this.liveMachine$ = combineLatest([this.machineService.getAll(), this.machineService.machineSampleEvents$]).pipe(
      map(combined => this.transform(combined[0], combined[1]))
    )
  }

  transform = (machines: Machine[], machineEventSample: MachineEventSample | null) => {
    if (!this.machineStore) {
      this.machineStore = [...machines];
    }

    if (machineEventSample && this.machineStore) {
      this.machineStore = <Machine[]>this.machineStore.map((machine: Machine) => {
        if (machineEventSample && machine.id === machineEventSample.id) {
          machine = {...machine, ...machineEventSample};
        }
        return machine;
      })
    }

    return this.machineStore;
  }
}
