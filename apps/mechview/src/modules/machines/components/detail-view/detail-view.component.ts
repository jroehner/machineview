import {Component, OnInit} from '@angular/core';
import {MachinesService} from '../../services/machines.service';
import {Machine} from '../../../../types/models/machine';
import {Observable} from 'rxjs';
import {ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'zeiss-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss']
})
export class DetailViewComponent implements OnInit {
  machine$: Observable<Machine> = new Observable<Machine>()

  displayedColumns: string[] = ['timestamp', 'status'];

  constructor(private activatedRoute: ActivatedRoute,
              private machinesService: MachinesService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) =>
      this.machine$ = this.machinesService.getOne(params?.id)
    );
  }
}
