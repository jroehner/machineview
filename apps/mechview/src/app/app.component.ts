import { Component } from '@angular/core';

@Component({
  selector: 'zeiss-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'mechview';
}
